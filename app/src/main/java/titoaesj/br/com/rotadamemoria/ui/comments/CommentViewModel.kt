package titoaesj.br.com.rotadamemoria.ui.comments

import android.arch.lifecycle.ViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import titoaesj.br.com.rotadamemoria.data.model.*
import titoaesj.br.com.rotadamemoria.data.repository.CoreRepository
import titoaesj.br.com.rotadamemoria.data.repository.UserRepository

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 31/08/2018.
 */
class CommentViewModel(private val coreTopicRepository: CoreRepository,
                       private val userRepository: UserRepository) : ViewModel() {

    fun getUserInSQLite(): Observable<User> = userRepository.getUserInSQLite()

    fun fetchCommentsAction(fileId : Int): Observable<CommentResult> = coreTopicRepository.fetchComments(fileId)

    fun storageCommentsAction(data: Comment): Observable<Result> = coreTopicRepository.storageCommentAction(data)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())

    fun removeUserInSQLite(): Observable<Unit>
            = userRepository.removeAllUserInSQLite()
}