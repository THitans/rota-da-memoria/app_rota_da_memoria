package titoaesj.br.com.rotadamemoria.di

import android.arch.persistence.room.Room
import android.content.Context
import com.github.salomonbrys.kodein.*
import com.github.salomonbrys.kodein.android.androidActivityScope
import titoaesj.br.com.rotadamemoria.data.repository.AuthRepository
import titoaesj.br.com.rotadamemoria.data.repository.CoreRepository
import titoaesj.br.com.rotadamemoria.data.repository.UserRepository
import titoaesj.br.com.rotadamemoria.data.sqlite.SnapshotsDatabase
import titoaesj.br.com.rotadamemoria.data.sqlite.dao.UserDAO
import titoaesj.br.com.rotadamemoria.data.webservice.AuthAPI
import titoaesj.br.com.rotadamemoria.data.webservice.CoreAPI
import titoaesj.br.com.rotadamemoria.data.webservice.WebServiceFactory
import titoaesj.br.com.rotadamemoria.ui.auth.SigninPresenter
import titoaesj.br.com.rotadamemoria.ui.auth.SigninViewModel
import titoaesj.br.com.rotadamemoria.ui.auth.SigninViewModelFactory
import titoaesj.br.com.rotadamemoria.ui.comments.CommentPresenter
import titoaesj.br.com.rotadamemoria.ui.comments.CommentViewModel
import titoaesj.br.com.rotadamemoria.ui.comments.CommentViewModelFactory
import titoaesj.br.com.rotadamemoria.ui.galery.GaleryPresenter
import titoaesj.br.com.rotadamemoria.ui.galery.GaleryViewModel
import titoaesj.br.com.rotadamemoria.ui.galery.GaleryViewModelFactory
import titoaesj.br.com.rotadamemoria.ui.subtopic.SubTopicPresenter
import titoaesj.br.com.rotadamemoria.ui.subtopic.SubTopicViewModel
import titoaesj.br.com.rotadamemoria.ui.subtopic.SubTopicViewModelFactory
import titoaesj.br.com.rotadamemoria.ui.topic.TopicPresenter
import titoaesj.br.com.rotadamemoria.ui.topic.TopicViewModel
import titoaesj.br.com.rotadamemoria.ui.topic.TopicViewModelFactory

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 21/07/2018.
 */
class Injection(private val context: Context) {

    val graph = Kodein.lazy {

        bind<SnapshotsDatabase>() with eagerSingleton {
            Room.databaseBuilder(context, SnapshotsDatabase::class.java, DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build()
        }


        bind<AuthAPI>() with eagerSingleton {
            WebServiceFactory
                    .build(debuggable = true, context = context).create(AuthAPI::class.java)
        }

        bind<CoreAPI>() with eagerSingleton {
            WebServiceFactory
                    .build(debuggable = true, context = context).create(CoreAPI::class.java)
        }


        bind<UserDAO>() with provider {
            val snapshotsDatabase : SnapshotsDatabase = instance()
            snapshotsDatabase.userDAO()
        }


        bind<AuthRepository>() with provider {
            AuthRepository(authAPI = instance())
        }

        bind<CoreRepository>() with provider {
            CoreRepository(coreAPI = instance())
        }

        bind<UserRepository>() with provider {
            UserRepository(userDAO = instance())
        }



        bind<SigninViewModelFactory>() with provider {
            SigninViewModelFactory(authRepository = instance(), userRepository = instance())
        }

        bind<TopicViewModelFactory>() with provider {
            TopicViewModelFactory(coreRepository = instance(),userRepository = instance())
        }

        bind<SubTopicViewModelFactory>() with provider {
            SubTopicViewModelFactory(coreRepository = instance(), userRepository = instance())
        }

        bind<GaleryViewModelFactory>() with provider {
            GaleryViewModelFactory(coreRepository = instance(), userRepository = instance())
        }

        bind<CommentViewModelFactory>() with provider {
            CommentViewModelFactory(coreRepository = instance(), userRepository = instance())
        }



        bind<SigninViewModel>() with provider {
            (instance() as SigninViewModelFactory).create(SigninViewModel::class.java)
        }

        bind<TopicViewModel>() with provider {
            (instance() as TopicViewModelFactory).create(TopicViewModel::class.java)
        }

        bind<SubTopicViewModel>() with provider {
            (instance() as SubTopicViewModelFactory).create(SubTopicViewModel::class.java)
        }

        bind<GaleryViewModel>() with provider {
            (instance() as GaleryViewModelFactory).create(GaleryViewModel::class.java)
        }

        bind<CommentViewModel>() with provider {
            (instance() as CommentViewModelFactory).create(CommentViewModel::class.java)
        }



        bind<SigninPresenter>() with scopedSingleton(androidActivityScope) {
            SigninPresenter(it, instance())
        }

        bind<TopicPresenter>() with scopedSingleton(androidActivityScope) {
            TopicPresenter(it, instance())
        }

        bind<SubTopicPresenter>() with scopedSingleton(androidActivityScope) {
            SubTopicPresenter(it, instance())
        }

        bind<GaleryPresenter>() with scopedSingleton(androidActivityScope) {
            GaleryPresenter(it, instance())
        }

        bind<CommentPresenter>() with scopedSingleton(androidActivityScope) {
            CommentPresenter(it, instance())
        }

    }


    companion object {
        val DATABASE_NAME = "rotaMemoria"
    }

}