package titoaesj.br.com.rotadamemoria.data.sqlite

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import titoaesj.br.com.rotadamemoria.data.model.User
import titoaesj.br.com.rotadamemoria.data.sqlite.dao.UserDAO

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 31/08/2018.
 */
@Database(
        version = 1,
        entities = [
            User::class
        ],
        exportSchema = false
)
abstract class SnapshotsDatabase : RoomDatabase() {

    abstract fun userDAO(): UserDAO

}
