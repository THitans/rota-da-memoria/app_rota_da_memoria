package titoaesj.br.com.rotadamemoria.ui.galery

import android.graphics.Bitmap
import android.support.v7.app.AlertDialog
import android.text.TextUtils
import android.view.View
import com.google.gson.GsonBuilder
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.HttpException
import timber.log.Timber
import titoaesj.br.com.rotadamemoria.data.model.UserResult
import titoaesj.br.com.rotadamemoria.helper.startActivityNoHistory
import titoaesj.br.com.rotadamemoria.ui.auth.SigninActivity
import titoaesj.br.com.rotadamemoria.ui.auth.SigninPresenter
import titoaesj.br.com.rotadamemoria.ui.topic.TopicActivity
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.*


/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 16/08/2018.
 */
class GaleryPresenter(view: Any, model: GaleryViewModel) {

    companion object {
        val TAG: String = GaleryPresenter::class.java.simpleName
    }

    private val mView: GaleryView = view as GaleryView
    private val mViewModel: GaleryViewModel = model

    private val compositeDisposable = CompositeDisposable()
    private var disposable: Disposable? = null

    fun cancelRequest() {
        compositeDisposable.clear()
    }

    fun upload(imageBitmap: Bitmap, subTopicId: Int) {
        val file: File = convertBitmapToFile(imageBitmap)
        val reqFile: RequestBody = RequestBody.create(MediaType.parse("image/*"), file)
        val body = MultipartBody.Part.createFormData("file", file.name, reqFile)




        disposable = mViewModel.upload(body, subTopicId)
                .subscribe(
                        { payload ->

                            Timber.tag(TAG).d("payload title ${payload}")
                            loadGalery(mView.SubTopicId())
                        },
                        { error ->

                            Timber.tag(TAG).d("payload title ${error.message}")

//                            val mapError = error as HttpException
//                            val errorBody = mapError.response().errorBody()?.string()
//
//                            val gson = GsonBuilder().setPrettyPrinting().create()
//                            val userResult = gson.fromJson<UserResult>(errorBody, UserResult::class.java)
//
//                            if (TextUtils.equals(userResult.message,"Token Expirado! Token has expired")) {
//                                (mView.Context() as TopicActivity).startActivityNoHistory<SigninActivity>()
//                            }
//
//                            val simpleAlert = AlertDialog.Builder(mView.Context()).create()
//                            simpleAlert.setTitle("Ops, Ocorreu um error")
//                            simpleAlert.setMessage(userResult.message)
//                            simpleAlert.setButton(AlertDialog.BUTTON_POSITIVE, "OK") { _, i ->
//                            }
//                            simpleAlert.show()

                        }
                )

        if (disposable != null) {
            compositeDisposable.add(disposable!!)
        }
    }


    private fun convertBitmapToFile(bitmap: Bitmap): File {

        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
        val filesDir: File = mView.Context().applicationContext.filesDir
        val imageFile = File(filesDir, timeStamp + ".png");

        var os: OutputStream

        os = FileOutputStream(imageFile)
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os)
        os.flush()
        os.close()

        return imageFile
    }

    fun loadGalery(subTopicId: Int) {

        startLoading()

        disposable = mViewModel.galery(subTopicId = subTopicId)
                .subscribe(
                        { payload ->
                            Timber.tag(TAG).d("payload ${payload.toString()}")
                            mView.Adapter().setAdapter(payload.files)

                            stopLoading()

                        },
                        { error ->

                            val mapError = error as HttpException
                            val errorBody = mapError.response().errorBody()?.string()

                            val gson = GsonBuilder().setPrettyPrinting().create()
                            val userResult = gson.fromJson<UserResult>(errorBody, UserResult::class.java)

                            if (TextUtils.equals(userResult.message, "Token Expirado! Token has expired")) {
                                (mView.Context() as TopicActivity).startActivityNoHistory<SigninActivity>()
                            }

                            val simpleAlert = AlertDialog.Builder(mView.Context()).create()
                            simpleAlert.setTitle("Ops, Ocorreu um error")
                            simpleAlert.setMessage(userResult.message)
                            simpleAlert.setButton(AlertDialog.BUTTON_POSITIVE, "OK") { _, i ->
                            }
                            simpleAlert.show()

                            stopLoading()

                        }
                )

        if (disposable != null) {
            compositeDisposable.add(disposable!!)
        }
    }

    private fun startLoading() {
        mView.loadingField().visibility = View.VISIBLE
    }

    private fun stopLoading() {
        mView.loadingField().visibility = View.GONE
    }

    fun getUserInSQLite() {
        disposable = mViewModel.getUserInSQLite()
                .subscribe {
                    mView.UsernameField().text = it.name
                }

        if (disposable != null) {
            compositeDisposable.add(disposable!!)
        }
    }

    fun removeUserInSQLite() {
        mViewModel.removeUserInSQLite()

    }

}