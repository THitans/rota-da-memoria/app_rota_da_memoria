package titoaesj.br.com.rotadamemoria.ui.topic

import android.arch.lifecycle.ViewModel
import io.reactivex.Observable
import titoaesj.br.com.rotadamemoria.data.model.Topic
import titoaesj.br.com.rotadamemoria.data.model.User
import titoaesj.br.com.rotadamemoria.data.repository.CoreRepository
import titoaesj.br.com.rotadamemoria.data.repository.UserRepository

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 03/08/2018.
 */
class TopicViewModel(private val coreTopicRepository: CoreRepository,
                     private val userRepository: UserRepository) : ViewModel() {

    fun fetchTopicsAction(): Observable<List<Topic>> = coreTopicRepository.fetchTopics()

    fun removeUserInSQLite(): Observable<Unit>
            = userRepository.removeAllUserInSQLite()

    fun getUserInSQLite(): Observable<User>
            = userRepository.getUserInSQLite()

}