package titoaesj.br.com.rotadamemoria.data.model

import com.google.gson.annotations.SerializedName

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 04/09/2018.
 */
class TokenResult(

        @SerializedName("message")
        var message: String? = "",

        @SerializedName("token")
        var token: String? = ""
)