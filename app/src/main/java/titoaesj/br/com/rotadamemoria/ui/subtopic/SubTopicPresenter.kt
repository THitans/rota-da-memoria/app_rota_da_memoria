package titoaesj.br.com.rotadamemoria.ui.subtopic

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import timber.log.Timber
import titoaesj.br.com.rotadamemoria.data.model.SubTopic
import titoaesj.br.com.rotadamemoria.ui.auth.SigninPresenter

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 03/08/2018.
 */
class SubTopicPresenter(view: Any, model: SubTopicViewModel) {

    companion object {
        val TAG: String = SubTopicPresenter::class.java.simpleName
    }

    private val mView: SubTopicView = view as SubTopicView
    private val mViewModel: SubTopicViewModel = model

    private val compositeDisposable = CompositeDisposable()
    private var disposable: Disposable? = null

    fun buildListSubTopics(data: List<SubTopic>) {
        Timber.tag(TAG).d("buildListSubTopics ${data.size}")
        mView.SubTopicAdapter()?.setAdapter(data)
    }

    fun getUserInSQLite() {
        disposable = mViewModel.getUserInSQLite()
                .subscribe {
                    mView.UsernameField().text = it.name
                }

        if (disposable != null) {
            compositeDisposable.add(disposable!!)
        }
    }

    fun removeUserInSQLite() {
        mViewModel.removeUserInSQLite()
    }

}

