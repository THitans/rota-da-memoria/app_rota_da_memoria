package titoaesj.br.com.rotadamemoria.ui.auth

import android.support.v7.app.AlertDialog
import android.text.TextUtils
import com.google.gson.GsonBuilder
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import retrofit2.HttpException
import timber.log.Timber
import titoaesj.br.com.rotadamemoria.R
import titoaesj.br.com.rotadamemoria.data.model.Result
import titoaesj.br.com.rotadamemoria.data.model.User
import titoaesj.br.com.rotadamemoria.data.model.UserResult
import titoaesj.br.com.rotadamemoria.data.sharedpreference.SessionHandler
import titoaesj.br.com.rotadamemoria.helper.startActivity
import titoaesj.br.com.rotadamemoria.ui.topic.TopicActivity

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 21/07/2018.
 */
class SigninPresenter(view: Any, model: SigninViewModel) {

    companion object {
        val TAG: String = SigninPresenter::class.java.simpleName
    }

    private val mView: SigninView = view as SigninView
    private val mViewModel: SigninViewModel = model

    private val compositeDisposable = CompositeDisposable()
    private var disposable: Disposable? = null

    fun cancelRequest() {
        compositeDisposable.clear()
    }

    fun isConnected() {
        if (SessionHandler.getUserID(mView.Context()) != null) {
            (mView.Context() as SigninActivity).startActivity<TopicActivity>()
        }
    }


    fun loginAction() {
        if (validationForm()) {

            mView.EmailFiled().editText?.text.toString()
            mView.PasswordField().editText?.text.toString()

            val user = User(email = mView.EmailFiled().editText?.text.toString(), password = mView.PasswordField().editText?.text.toString())

            disposable = mViewModel.loginAction(user)
                    .subscribe(
                            { payload ->

                                mViewModel.removeUserInSQLite()

                                disposable = mViewModel.removeUserInSQLite()
                                        .subscribe { it ->

                                            disposable = mViewModel.storageUserInSQLite(payload.user!!)
                                                    .subscribe {
                                                        Timber.tag(TAG).d("loginAction name ${payload.user!!.name!!}")
                                                        SessionHandler.setUserID(mView.Context(), payload.user!!.id!!)
                                                        SessionHandler.setToken(mView.Context(), payload.token!!)
                                                        SessionHandler.setUserName(mView.Context(), payload.user!!.name!!)
                                                        (mView.Context() as SigninActivity).startActivity<TopicActivity>()
                                                    }

                                            if (disposable != null) {
                                                compositeDisposable.add(disposable!!)
                                            }

                                        }

                                if (disposable != null) {
                                    compositeDisposable.add(disposable!!)
                                }
                            },
                            { error ->

                                try {
                                    val mapError = error as HttpException
                                    val errorBody = mapError.response().errorBody()?.string()

                                    val gson = GsonBuilder().setPrettyPrinting().create()
                                    val userResult = gson.fromJson(errorBody, Result::class.java)

                                    val simpleAlert = AlertDialog.Builder(mView.Context()).create()
                                    simpleAlert.setTitle("Ops, Ocorreu um error")
                                    simpleAlert.setMessage(userResult.message)
                                    simpleAlert.setButton(AlertDialog.BUTTON_POSITIVE, "OK") { _, i ->
                                    }
                                    simpleAlert.show()
                                } catch (e : Exception) {
                                    Timber.tag(TAG).e("error ${e.message}")

                                    val simpleAlert = AlertDialog.Builder(mView.Context()).create()
                                    simpleAlert.setTitle("Ops, Ocorreu um error")
                                    simpleAlert.setMessage("Ocorreu um error no login.")
                                    simpleAlert.setButton(AlertDialog.BUTTON_POSITIVE, "OK") { _, i ->
                                    }
                                    simpleAlert.show()
                                }
                            }
                    )

            if (disposable != null) {
                compositeDisposable.add(disposable!!)
            }
        }
    }


    fun validationForm(): Boolean {

        var check = true

        mView.EmailFiled().error = ""
        mView.PasswordField().error = ""

        if (TextUtils.isEmpty((mView.EmailFiled().editText?.text))) {
            mView.EmailFiled().error = mView.Context().getString(R.string.empty_message_default)
            check = false
        }

        if (TextUtils.isEmpty((mView.PasswordField().editText?.text))) {
            mView.PasswordField().error = mView.Context().getString(R.string.empty_message_default)
            check = false
        } else if (mView.PasswordField().editText?.text.toString().length < 6) {
            mView.PasswordField().error = mView.Context().getString(R.string.empty_message_error_password_lenght_6)
            check = false
        }

        return check
    }
}

