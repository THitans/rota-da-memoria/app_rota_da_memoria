package titoaesj.br.com.rotadamemoria.data.model

import com.google.gson.annotations.SerializedName

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 17/08/2018.
 */
data class Galery(

        @SerializedName("basePath")
        var basePath: String = "",


        @SerializedName("files")
        var files: List<File> = emptyList()

)