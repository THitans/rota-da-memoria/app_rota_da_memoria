package titoaesj.br.com.rotadamemoria.ui.topic

import android.content.Context
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.widget.ProgressBar

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 03/08/2018.
 */
interface TopicView {
    fun Context(): Context
    fun TopicListField(): RecyclerView
    fun loadingField(): ProgressBar
    fun UsernameField() : AppCompatTextView
    fun TopicAdapter(): RVAdapterTopic?
}