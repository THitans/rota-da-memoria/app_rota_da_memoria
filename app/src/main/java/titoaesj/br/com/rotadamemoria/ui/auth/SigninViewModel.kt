package titoaesj.br.com.rotadamemoria.ui.auth

import android.arch.lifecycle.ViewModel
import io.reactivex.Observable
import titoaesj.br.com.rotadamemoria.data.model.User
import titoaesj.br.com.rotadamemoria.data.model.UserResult
import titoaesj.br.com.rotadamemoria.data.repository.AuthRepository
import titoaesj.br.com.rotadamemoria.data.repository.UserRepository

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 21/07/2018.
 */
class SigninViewModel(private val authRepository: AuthRepository,
                      private val userRepository: UserRepository) : ViewModel() {

    fun loginAction(data: User): Observable<UserResult> = authRepository.login(data)

    fun storageUserInSQLite(data: User): Observable<Unit>
            = userRepository.storageUserInSQLite(data)

    fun removeUserInSQLite(): Observable<Unit>
            = userRepository.removeAllUserInSQLite()

}