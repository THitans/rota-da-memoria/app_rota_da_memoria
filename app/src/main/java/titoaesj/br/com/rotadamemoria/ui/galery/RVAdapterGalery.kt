package titoaesj.br.com.rotadamemoria.ui.galery

import android.os.Bundle
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.squareup.picasso.Picasso
import titoaesj.br.com.rotadamemoria.R
import titoaesj.br.com.rotadamemoria.data.model.File

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 21/07/2018.
 */
class RVAdapterGalery() : RecyclerView.Adapter<RVAdapterGalery.ViewHolder>() {

    companion object {
        val TAG: String = RVAdapterGalery::class.java.simpleName
    }

    val itensOfList: MutableList<File> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RVAdapterGalery.ViewHolder {
        val v = LayoutInflater
                .from(parent.context)
                .inflate(R.layout.recyclerview_galery_item, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: RVAdapterGalery.ViewHolder, position: Int) {
        holder.bindItems(itensOfList[position])
    }

    override fun getItemCount(): Int {
        return itensOfList.size
    }

    fun clearAdapter() {
        itensOfList.clear()
        notifyDataSetChanged()
    }

    fun addItemAdapter(item: File) {
        itensOfList.add(item)
        notifyItemChanged(itensOfList.size - 1)
    }

    fun setAdapter(itens: List<File>) {
        itensOfList.clear()
        itensOfList.addAll(itens)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(item: File) {

            val image = itemView.findViewById<AppCompatImageView>(R.id.recyclerview_galery_item)

            Log.d(TAG, "" + item.toString())

            /**
             * Redimensiona a imagem
             */
//            val transformation: Transformation = object : Transformation {
//
//                override fun transform(source: Bitmap?): Bitmap? {
//                    val targetWidth = source!!.width
//                    val aspectRatio = source.height.toDouble() / source.width.toDouble()
//                    val targetHeight = (targetWidth * aspectRatio).toInt()
//                    val result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false)
//                    if (result != source) {
//                        source.recycle()
//                    }
//                    return result
//                }
//
//                override fun key(): String {
//                    return "transformation" + " desiredWidth"
//                }
//            }

            image.setOnClickListener {
                (itemView.context as GaleryActivity).launchToCommentActivity(item)
            }

            Picasso.with(itemView.context)
                    .load("http://rota-memoria.thitans.com/storage/${item.pathFile}")
                    .fit()
                    .centerCrop()
                    .into(image)
        }
    }
}
