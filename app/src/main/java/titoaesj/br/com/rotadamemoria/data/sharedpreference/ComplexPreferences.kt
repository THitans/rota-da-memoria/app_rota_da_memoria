package titoaesj.br.com.rotadamemoria.data.sharedpreference

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.support.v4.content.SharedPreferencesCompat
import com.google.gson.Gson
import titoaesj.br.com.rotadamemoria.R
import java.util.logging.Level
import java.util.logging.Logger

/**
 * Project Bely
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 05/10/17.
 */

class ComplexPreferences @SuppressLint("CommitPrefEdits")
private constructor(context: Context, namePreferences: String, mode: Int) {

    private val logger = Logger.getLogger(ComplexPreferences::class.java.name)
    private val mSharedPreferences: SharedPreferences
    private val mEditor: SharedPreferences.Editor
    private val mGson = Gson()

    init {

        val mTagPreference: String

        if (namePreferences.isEmpty()) {
            mTagPreference = context.getString(R.string.spref_complex_preference)
        } else {
            mTagPreference = namePreferences
        }

        this.mSharedPreferences = context.getSharedPreferences(mTagPreference, mode)
        this.mEditor = this.mSharedPreferences.edit()
    }

    /**
     * Adiciona um objeto associado a uma chave no SharePreference.
     *
     * @param key    chave do objeto
     * @param object objeto a ser armazenado
     */
    @SuppressLint("CommitPrefEdits")
    internal fun putObject(key: String?, `object`: Any?) {
        if (`object` == null) {
            throw IllegalArgumentException("Ocorreu um erro, objeto a ser salvo é null.")
        }
        if (key != null && key.isEmpty()) {
            throw IllegalArgumentException("Ocorreu um erro, chave é nula ou esta vazia.")
        }
        this.mEditor.putString(key, mGson.toJson(`object`))
        SharedPreferencesCompat.EditorCompat.getInstance().apply(this.mEditor)
    }

    /**
     * Remove objeto do SharePreference por meio de sua chave.
     *
     * @param key chave do objeto
     */
    @SuppressLint("CommitPrefEdits")
    internal fun removeObject(key: String) {
        this.mEditor.remove(key)
        SharedPreferencesCompat.EditorCompat.getInstance().apply(this.mEditor)
    }

    /**
     * Retorna um objeto armazenado no SharePreference por meio de sua chave.
     *
     * @param key      chave do objeto
     * @param a        objeto para o cast do objeto genérica
     * @param <T>class oara o cast da classe genérica
     * @return retorna um objeto da classe passada.
    </T> */
    internal fun <T> getObject(key: String, a: Class<T>): T? {
        val gson = mSharedPreferences.getString(key, null)
        return if (gson == null) {
            null
        } else {
            try {
                mGson.fromJson(gson, a)
            } catch (e: Exception) {
                logger.log(Level.SEVERE, "Ocorreu um erro, ao tentar realizar a serialização do json. ", e)
                throw IllegalArgumentException("Ocorreu um erro, ao tentar realizar a serialização do json.")
            }

        }
    }

    companion object {
        private var complexPreferences: ComplexPreferences? = null

        internal fun getComplexPreferences(context: Context,
                                           namePreferences: String,
                                           mode: Int): ComplexPreferences {
            if (complexPreferences == null) {
                complexPreferences = ComplexPreferences(context, namePreferences, mode)
            }
            return complexPreferences!!
        }
    }

}
