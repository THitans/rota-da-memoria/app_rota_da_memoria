package titoaesj.br.com.rotadamemoria.data.sqlite.dao

import android.arch.persistence.room.*
import titoaesj.br.com.rotadamemoria.data.model.User

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 31/08/2018.
 */
@Dao
interface UserDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: User)

    @Update
    fun update(user: User)

    @Delete
    fun delete(user: User)

    @Query("SELECT * FROM user LIMIT 1")
    fun user(): User

    @Query("DELETE FROM user")
    fun deleteAll()

}