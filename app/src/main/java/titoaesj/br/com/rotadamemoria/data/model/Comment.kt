package titoaesj.br.com.rotadamemoria.data.model

import com.google.gson.annotations.SerializedName

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 31/08/2018.
 */
data class Comment(

        @SerializedName("file_name")
        var id: Int = 0,

        @SerializedName("text")
        var text: String = "",

        @SerializedName("file_id")
        var fileId: Int = 0,

        @SerializedName("user_id")
        var userId: Int = 0,

        @SerializedName("created_at")
        var createddAt: String = "",

        @SerializedName("updated_at")
        var updateddAt: String = "",

        @SerializedName("deleted_at")
        var deletedAt: String = "",

        @SerializedName("user")
        var user: User? = null
)