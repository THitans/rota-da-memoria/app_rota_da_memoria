package titoaesj.br.com.rotadamemoria.ui.subtopic

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import titoaesj.br.com.rotadamemoria.data.repository.CoreRepository
import titoaesj.br.com.rotadamemoria.data.repository.UserRepository

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 03/08/2018.
 */
class SubTopicViewModelFactory(private val coreRepository: CoreRepository,
                               private val userRepository: UserRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SubTopicViewModel::class.java)) {
            return SubTopicViewModel(coreRepository, userRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}