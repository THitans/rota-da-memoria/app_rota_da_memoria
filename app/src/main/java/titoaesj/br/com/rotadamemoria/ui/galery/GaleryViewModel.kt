package titoaesj.br.com.rotadamemoria.ui.galery

import android.arch.lifecycle.ViewModel
import com.google.gson.JsonObject
import io.reactivex.Observable
import okhttp3.MultipartBody
import titoaesj.br.com.rotadamemoria.data.model.Galery
import titoaesj.br.com.rotadamemoria.data.model.User
import titoaesj.br.com.rotadamemoria.data.repository.CoreRepository
import titoaesj.br.com.rotadamemoria.data.repository.UserRepository

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 16/08/2018.
 */
class GaleryViewModel(private val coreTopicRepository: CoreRepository,
                      private val userRepository: UserRepository) : ViewModel() {

    fun upload(file: MultipartBody.Part, subTopicId: Int): Observable<JsonObject>
            = coreTopicRepository.upload(file, subTopicId)

    fun galery(subTopicId: Int): Observable<Galery>
            = coreTopicRepository.galery(subTopicId)

    fun getUserInSQLite(): Observable<User>
            = userRepository.getUserInSQLite()

    fun removeUserInSQLite(): Observable<Unit>
            = userRepository.removeAllUserInSQLite()

}