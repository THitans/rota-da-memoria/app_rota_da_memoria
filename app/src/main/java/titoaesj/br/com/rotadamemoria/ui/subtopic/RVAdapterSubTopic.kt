package titoaesj.br.com.rotadamemoria.ui.subtopic

import android.content.Intent
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import timber.log.Timber
import titoaesj.br.com.rotadamemoria.R
import titoaesj.br.com.rotadamemoria.data.model.SubTopic
import titoaesj.br.com.rotadamemoria.ui.galery.GaleryActivity

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 21/07/2018.
 */
class RVAdapterSubTopic : RecyclerView.Adapter<RVAdapterSubTopic.ViewHolder>() {

    companion object {
        val TAG: String = RVAdapterSubTopic::class.java.simpleName
    }

    private val itensOfList: MutableList<SubTopic> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater
                .from(parent.context)
                .inflate(R.layout.recyclerview_sub_topic_item, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(itensOfList[position])
    }

    override fun getItemCount(): Int {
        return itensOfList.size
    }

    fun clearAdapter() {
        itensOfList.clear()
        notifyDataSetChanged()
    }

    fun addItemAdapter(item: SubTopic) {
        itensOfList.add(item)
        notifyItemChanged(itensOfList.size - 1)
    }

    fun setAdapter(itens: List<SubTopic>) {

        Timber.tag(TAG).d("setAdapter ${itens.size}")

        itensOfList.clear()
        itensOfList.addAll(itens)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(subTopic: SubTopic) {

            Timber.tag(TAG).d("subtopics subTopic ${subTopic}")

            val button = itemView.findViewById<AppCompatTextView>(R.id.recyclerview_sub_topic_item_title)
            val cardView = itemView.findViewById<CardView>(R.id.recyclerview_sub_topic_item_cardview)
            button.text = subTopic.title

            cardView.setOnClickListener {
                val intent = Intent(itemView.context, GaleryActivity::class.java)
                intent.putExtra(itemView.resources.getString(R.string.TAG_BUNDLE_GALERY_TITLE), subTopic.title)
                intent.putExtra(itemView.resources.getString(R.string.TAG_BUNDLE_TOPIC_ID), subTopic.id)
                itemView.context.startActivity(intent)
            }
        }
    }
}
