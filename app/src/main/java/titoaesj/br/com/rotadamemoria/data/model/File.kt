package titoaesj.br.com.rotadamemoria.data.model

import com.google.gson.annotations.SerializedName

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 28/08/2018.
 */
data class File(

        @SerializedName("id")
        var id: Int = -1,

        @SerializedName("file_name")
        var fileName: String = "",

        @SerializedName("path_file")
        var pathFile: String = "",

        @SerializedName("created_at")
        var createdAt: String = "",

        @SerializedName("deleted_at")
        var deletedAt: String = ""
)