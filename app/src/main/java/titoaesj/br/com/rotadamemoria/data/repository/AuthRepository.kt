package titoaesj.br.com.rotadamemoria.data.repository

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import titoaesj.br.com.rotadamemoria.data.model.User
import titoaesj.br.com.rotadamemoria.data.model.UserResult
import titoaesj.br.com.rotadamemoria.data.webservice.AuthAPI

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 21/07/2018.
 */
class AuthRepository(
        val authAPI: AuthAPI) {

    /**
     * Realiza o login do usuário
     * utilizando Uusuário e Senha
     */
    fun login(data: User): Observable<UserResult> =
            authAPI.login(data)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())

    fun refresh(token: String): Observable<UserResult> =
            authAPI.refresh(token)

}