package titoaesj.br.com.rotadamemoria.data.sharedpreference

import android.content.Context
import titoaesj.br.com.rotadamemoria.R

/**
 * Project Bely
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 05/10/17.
 */

object SessionHandler {

    fun getUserID(context: Context): Int? {
        val complexPreferences = ComplexPreferences.getComplexPreferences(context,
                context.getString(R.string.spref_data_session_handler_defaulter), Context.MODE_PRIVATE)
        return complexPreferences.getObject(context.getString(R.string.spref_data_session_handler_account), Int::class.java)
    }
    fun setUserID(context: Context, clientID: Int) {
        val complexPreferences = ComplexPreferences.getComplexPreferences(context,
                context.getString(R.string.spref_data_session_handler_defaulter), Context.MODE_PRIVATE)

        complexPreferences.putObject(context.getString(R.string.spref_data_session_handler_account), clientID)
    }
    fun removeUserID(context: Context) {
        val complexPreferences = ComplexPreferences.getComplexPreferences(context,
                context.getString(R.string.spref_data_session_handler_defaulter), Context.MODE_PRIVATE)
        complexPreferences.removeObject(context.getString(R.string.spref_data_session_handler_account))
    }

    fun getUserName(context: Context): String? {
        val complexPreferences = ComplexPreferences.getComplexPreferences(context,
                context.getString(R.string.spref_data_session_handler_defaulter), Context.MODE_PRIVATE)
        return complexPreferences.getObject(context.getString(R.string.spref_data_session_handler_account_name), String::class.java)
    }
    fun setUserName(context: Context, clientID: String) {
        val complexPreferences = ComplexPreferences.getComplexPreferences(context,
                context.getString(R.string.spref_data_session_handler_defaulter), Context.MODE_PRIVATE)

        complexPreferences.putObject(context.getString(R.string.spref_data_session_handler_account_name), clientID)
    }
    fun removeUserName(context: Context) {
        val complexPreferences = ComplexPreferences.getComplexPreferences(context,
                context.getString(R.string.spref_data_session_handler_defaulter), Context.MODE_PRIVATE)
        complexPreferences.removeObject(context.getString(R.string.spref_data_session_handler_account_name))
    }

    fun getToken(context: Context): String? {
        val complexPreferences = ComplexPreferences.getComplexPreferences(context,
                context.getString(R.string.spref_data_session_handler_defaulter), Context.MODE_PRIVATE)
        return complexPreferences.getObject(context.getString(R.string.spref_data_session_handler_token), String::class.java)
    }
    fun setToken(context: Context, clientID: String) {
        val complexPreferences = ComplexPreferences.getComplexPreferences(context,
                context.getString(R.string.spref_data_session_handler_defaulter), Context.MODE_PRIVATE)

        complexPreferences.putObject(context.getString(R.string.spref_data_session_handler_token), clientID)
    }
    fun removeToken(context: Context) {
        val complexPreferences = ComplexPreferences.getComplexPreferences(context,
                context.getString(R.string.spref_data_session_handler_defaulter), Context.MODE_PRIVATE)
        complexPreferences.removeObject(context.getString(R.string.spref_data_session_handler_token))
    }
}
