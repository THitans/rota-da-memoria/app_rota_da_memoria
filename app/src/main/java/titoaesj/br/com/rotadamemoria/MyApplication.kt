package titoaesj.br.com.rotadamemoria

import android.app.Application
import android.support.v7.app.AppCompatDelegate
import com.github.salomonbrys.kodein.KodeinAware
import com.jakewharton.threetenabp.AndroidThreeTen
import timber.log.Timber
import titoaesj.br.com.rotadamemoria.di.Injection

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 21/07/2018.
 */
class MyApplication : Application(), KodeinAware {


    companion object {
        lateinit var instance: MyApplication
    }

    override fun onCreate() {
        super.onCreate()

        AndroidThreeTen.init(this)

        /**
         * Inicializa / planta o Timber para os
         * Log`s em desenvolvimento
         */
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        instance = this

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }

    fun getInstance(): MyApplication {
        return instance
    }

    override val kodein by Injection(this).graph
}