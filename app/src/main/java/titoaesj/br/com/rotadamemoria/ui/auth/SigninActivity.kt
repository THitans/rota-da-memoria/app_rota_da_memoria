package titoaesj.br.com.rotadamemoria.ui.auth

import android.content.Context
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.support.v7.app.AppCompatActivity
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import kotlinx.android.synthetic.main.activity_signin.*
import titoaesj.br.com.rotadamemoria.R

class SigninActivity : AppCompatActivity(), SigninView {

    private val kodein by lazy { LazyKodein(appKodein) }
    private val mPresenter: SigninPresenter by kodein.with(this).instance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)

        /**
         * Verifica se o usuário já esta conectado,
         * caso estaja redireciona para tela de Tópicos.
         */
        mPresenter.isConnected()

        activity_signin_appcompatbutton_signin.setOnClickListener {
            mPresenter.loginAction()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.cancelRequest()
    }

    override fun Context(): Context {
        return this
    }

    override fun EmailFiled(): TextInputLayout {
        return activity_signin_textinputlayout_email
    }

    override fun PasswordField(): TextInputLayout {
        return activity_signin_textinputlayout_password
    }
}
