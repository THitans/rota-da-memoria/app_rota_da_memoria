package titoaesj.br.com.rotadamemoria.ui.comments

import android.support.v7.app.AlertDialog
import android.text.TextUtils
import android.view.View
import com.google.gson.GsonBuilder
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import retrofit2.HttpException
import timber.log.Timber
import titoaesj.br.com.rotadamemoria.data.model.Comment
import titoaesj.br.com.rotadamemoria.data.model.UserResult
import titoaesj.br.com.rotadamemoria.data.sharedpreference.SessionHandler
import titoaesj.br.com.rotadamemoria.helper.startActivityNoHistory
import titoaesj.br.com.rotadamemoria.ui.auth.SigninActivity
import titoaesj.br.com.rotadamemoria.ui.topic.TopicActivity
import titoaesj.br.com.rotadamemoria.ui.topic.TopicPresenter

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 31/08/2018.
 */
class CommentPresenter(view: Any, model: CommentViewModel) {

    companion object {
        val TAG: String = CommentPresenter::class.java.simpleName
    }

    private val mView: CommentView = view as CommentView
    private val mViewModel: CommentViewModel = model

    private val compositeDisposable = CompositeDisposable()
    private var disposable: Disposable? = null

    fun cancelRequest() {
        compositeDisposable.clear()
    }

    fun getUserInSQLite(fileId : Int) {
        disposable = mViewModel.getUserInSQLite()
                .subscribe {
                    getCommentsAction(fileId)
                    mView.UsernameField().text = it.name
                    SessionHandler.setUserName(mView.Context(),it.name!!)
                }

        if (disposable != null) {
            compositeDisposable.add(disposable!!)
        }
    }

    fun getCommentsAction(fileId : Int) {
        startLoading()

        disposable = mViewModel.fetchCommentsAction(fileId)
                .subscribe(
                        { payload ->

                            mView.CommentAdapter()?.setAdapter(payload.comments)

                            stopLoading()
                        },
                        { error ->


                            Timber.tag(TAG).d("ocorreu um error ${error.message}")

                            val mapError = error as HttpException
                            val errorBody = mapError.response().errorBody()?.string()

                            val gson = GsonBuilder().setPrettyPrinting().create()
                            val userResult = gson.fromJson<UserResult>(errorBody, UserResult::class.java)

                            if (TextUtils.equals(userResult.message,"Token Expirado! Token has expired")) {
                                SessionHandler.removeToken(mView.Context())
                                SessionHandler.removeUserID(mView.Context())
                                (mView.Context() as TopicActivity).startActivityNoHistory<SigninActivity>()
                            }

                            val simpleAlert = AlertDialog.Builder(mView.Context()).create()
                            simpleAlert.setTitle("Ops, Ocorreu um error")
                            simpleAlert.setMessage(userResult.message)
                            simpleAlert.setButton(AlertDialog.BUTTON_POSITIVE, "OK") { _, i ->
                            }
                            simpleAlert.show()

                            stopLoading()
                        }
                )

        if (disposable != null) {
            compositeDisposable.add(disposable!!)
        }
    }

    private fun stopLoading() {
        mView.loadingField().visibility = View.GONE

    }

    private fun startLoading() {
        mView.loadingField().visibility = View.VISIBLE

    }

    fun storageCommentAction(text: String, fileId: Int) {

        disposable = mViewModel.storageCommentsAction(Comment(text = text,fileId = fileId, userId = SessionHandler.getUserID(mView.Context())!!.toInt()))
                .subscribe(
                        {
                            getCommentsAction(fileId)
                        },
                        { error ->
                            Timber.tag(TAG).e("error ${error.message}")
                        }
                )

        if (disposable != null) {
            compositeDisposable.add(disposable!!)
        }
    }

    fun removeUserInSQLite() {
        mViewModel.removeUserInSQLite()

    }
}