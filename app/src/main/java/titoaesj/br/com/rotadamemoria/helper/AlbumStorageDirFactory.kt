package titoaesj.br.com.rotadamemoria.helper

import java.io.File

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 15/08/2018.
 */
abstract class AlbumStorageDirFactory {
    abstract fun getAlbumStorageDir(albumName: String): File
}