package titoaesj.br.com.rotadamemoria.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 21/07/2018.
 */
data class Topic(

        @SerializedName("id")
        var id: Long = 0L,

        @SerializedName("text")
        var title: String = "",

        @SerializedName("is_published")
        var isPublished: Boolean = true,

        @SerializedName("created_at")
        var createdAt: String = "",

        @SerializedName("updated_at")
        var updatedAt: String = "",

        @SerializedName("deleted_at")
        var deletedAt: String = "",

        @SerializedName("sub_topics")
        var subTopics: List<SubTopic> = emptyList()

) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readLong(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readString(),
            parcel.readString(),
            (parcel.readString() != "").toString(),
            parcel.createTypedArrayList(SubTopic)!!) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(title)
        parcel.writeByte(if (isPublished) 1 else 0)
        parcel.writeString(createdAt)
        parcel.writeString(updatedAt)
        parcel.writeString(deletedAt)
        parcel.writeTypedList(subTopics)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Topic> {
        override fun createFromParcel(parcel: Parcel): Topic {
            return Topic(parcel)
        }

        override fun newArray(size: Int): Array<Topic?> {
            return arrayOfNulls(size)
        }
    }

}