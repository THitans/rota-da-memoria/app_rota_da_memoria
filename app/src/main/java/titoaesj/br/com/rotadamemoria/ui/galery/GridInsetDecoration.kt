package titoaesj.br.com.rotadamemoria.ui.galery

import android.content.Context
import android.graphics.Rect
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import titoaesj.br.com.rotadamemoria.R


/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 15/08/2018.
 */
class GridInsetDecoration(context: Context) : RecyclerView.ItemDecoration() {

    private val insetHorizontal: Int
    private val insetVertical: Int

    init {
        insetHorizontal = context.getResources()
                .getDimensionPixelSize(R.dimen.grid_horizontal_spacing)
        insetVertical = context.getResources()
                .getDimensionPixelOffset(R.dimen.grid_vertical_spacing)
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                                state: RecyclerView.State) {
        val layoutParams = view.getLayoutParams() as GridLayoutManager.LayoutParams

        val position = layoutParams.viewPosition
        if (position == RecyclerView.NO_POSITION) {
            outRect.set(0, 0, 0, 0)
            return
        }

        // add edge margin only if item edge is not the grid edge
        val itemSpanIndex = layoutParams.spanIndex
        // is left grid edge?
        outRect.left = if (itemSpanIndex == 0) 0 else insetHorizontal
        // is top grid edge?
        outRect.top = if (itemSpanIndex == position) 0 else insetVertical
        outRect.right = 0
        outRect.bottom = 0
    }
}