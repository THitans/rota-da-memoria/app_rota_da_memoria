package titoaesj.br.com.rotadamemoria.ui.comments

import android.content.Context
import android.support.design.widget.CoordinatorLayout
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.widget.ProgressBar

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 31/08/2018.
 */
interface CommentView {
    fun UsernameField(): AppCompatTextView
    fun CommentListField(): RecyclerView
    fun Context(): Context
    fun loadingField(): ProgressBar
    fun CommentAdapter(): RVAdapterComment?

}