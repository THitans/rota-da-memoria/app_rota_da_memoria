package titoaesj.br.com.rotadamemoria.ui.galery

import android.content.Context
import android.support.v7.widget.AppCompatTextView
import android.widget.ProgressBar

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 16/08/2018.
 */
interface GaleryView {
    fun Context(): Context
    fun Adapter() : RVAdapterGalery
    fun SubTopicId(): Int
    fun loadingField(): ProgressBar
    fun UsernameField(): AppCompatTextView


}