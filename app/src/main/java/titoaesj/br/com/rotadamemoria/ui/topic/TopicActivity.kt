package titoaesj.br.com.rotadamemoria.ui.topic

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.LinearLayout
import android.widget.ProgressBar
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import kotlinx.android.synthetic.main.activity_topic.*
import kotlinx.android.synthetic.main.base_layout_appbarlayout.*
import titoaesj.br.com.rotadamemoria.R
import titoaesj.br.com.rotadamemoria.data.sharedpreference.SessionHandler
import titoaesj.br.com.rotadamemoria.helper.startActivityNoHistory
import titoaesj.br.com.rotadamemoria.ui.auth.SigninActivity

class TopicActivity : AppCompatActivity(), TopicView {

    private val kodein by lazy { LazyKodein(appKodein) }
    private val mPresenter: TopicPresenter by kodein.with(this).instance()

    private var rvAdapterTopic : RVAdapterTopic? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_topic)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

//        SessionHandler.removeUserID(this)
//        SessionHandler.removeToken(this)
//        SessionHandler.removeUserName(this)
//        mPresenter.removeUserInSQLite()

        activity_topic_recyclerview_topics.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        rvAdapterTopic = RVAdapterTopic()
        TopicListField().adapter = rvAdapterTopic
        mPresenter.getTopicsAction()

    }

    override fun onStart() {
        super.onStart()
        mPresenter.getUserInSQLite()
    }

    override fun Context(): Context {
        return this
    }

    override fun onDestroy() {
        super.onDestroy()
        mPresenter.cancelRequest()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = MenuInflater(this)
        inflater.inflate(R.menu.logout_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {

            android.R.id.home -> {
                onBackPressed()
                return true
            }
            R.id.menu_logout -> {
                SessionHandler.removeToken(this)
                SessionHandler.removeUserID(this)
                mPresenter.removeUserInSQLite()
                startActivityNoHistory<SigninActivity>()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun TopicListField(): RecyclerView = activity_topic_recyclerview_topics
    override fun loadingField(): ProgressBar = activity_topic_loading_progressbar
    override fun UsernameField(): AppCompatTextView = base_layout_appbarlayout_username
    override fun TopicAdapter(): RVAdapterTopic?  = rvAdapterTopic
}