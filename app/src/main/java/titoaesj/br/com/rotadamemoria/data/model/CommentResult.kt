package titoaesj.br.com.rotadamemoria.data.model

import com.google.gson.annotations.SerializedName

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 01/09/2018.
 */
class CommentResult(

        @SerializedName("comments")
        var comments: List<Comment> = emptyList()
)