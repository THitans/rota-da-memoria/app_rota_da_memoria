package titoaesj.br.com.rotadamemoria.helper

import android.os.Environment
import java.io.File

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 15/08/2018.
 */
class BaseAlbumDirFactory : AlbumStorageDirFactory() {

    override fun getAlbumStorageDir(albumName: String): File {
        return File(
                (Environment.getExternalStorageDirectory()).toString()
                        + CAMERA_DIR
                        + albumName
        )
    }

    companion object {
        // Standard storage location for digital camera files
        private val CAMERA_DIR = "/dcim/"
    }
}
