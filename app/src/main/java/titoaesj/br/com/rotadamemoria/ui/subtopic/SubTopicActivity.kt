package titoaesj.br.com.rotadamemoria.ui.subtopic

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.LinearLayout
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import kotlinx.android.synthetic.main.activity_sub_topic.*
import kotlinx.android.synthetic.main.base_layout_appbarlayout.*
import kotlinx.android.synthetic.main.base_layout_appbarlayout.view.*
import timber.log.Timber
import titoaesj.br.com.rotadamemoria.R
import titoaesj.br.com.rotadamemoria.data.model.Topic
import titoaesj.br.com.rotadamemoria.data.sharedpreference.SessionHandler
import titoaesj.br.com.rotadamemoria.helper.startActivityNoHistory
import titoaesj.br.com.rotadamemoria.ui.auth.SigninActivity


class SubTopicActivity : AppCompatActivity(), SubTopicView {



    private val kodein by lazy { LazyKodein(appKodein) }
    private val mPresenter: SubTopicPresenter by kodein.with(this).instance()

    private var rvAdapterSubTopic: RVAdapterSubTopic? = null

    companion object {
        val TAG: String = SubTopicActivity::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sub_topic)

        setSupportActionBar(activity_sub_topic_appbarlayout.toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        activity_topic_recyclerview_sub_topics.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        rvAdapterSubTopic = RVAdapterSubTopic()
        SubTopicListField().adapter = rvAdapterSubTopic



        if (intent.hasExtra(resources.getString(R.string.TAG_BUNDLE_TOPIC))) {
            val topic: Topic = intent.getParcelableExtra(resources.getString(R.string.TAG_BUNDLE_TOPIC))
            activity_sub_topic_appbarlayout.toolbar.title = topic.title

//            val gson = Gson()
//            val jsonString = gson.toJson(topic.subTopics.toString())
//            val listType = object : TypeToken<List<SubTopic>>() { }.type
//            val newList = gson.fromJson<List<SubTopic>>(jsonString, listType)

            mPresenter.buildListSubTopics(topic.subTopics)
        }

    }

    override fun onStart() {
        super.onStart()

        mPresenter.getUserInSQLite()
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = MenuInflater(this)
        inflater.inflate(R.menu.logout_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            R.id.menu_logout -> {
                SessionHandler.removeToken(this)
                SessionHandler.removeUserID(this)
                mPresenter.removeUserInSQLite()
                startActivityNoHistory<SigninActivity>()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun Context(): Context = this
    override fun SubTopicListField(): RecyclerView = activity_topic_recyclerview_sub_topics
    override fun UsernameField(): AppCompatTextView = base_layout_appbarlayout_username
    override fun SubTopicAdapter(): RVAdapterSubTopic? = rvAdapterSubTopic

}

