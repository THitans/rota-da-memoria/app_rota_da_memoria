package titoaesj.br.com.rotadamemoria.data.webservice

import android.content.Context
import com.google.gson.Gson
import okhttp3.*
import timber.log.Timber
import titoaesj.br.com.rotadamemoria.data.model.TokenResult
import titoaesj.br.com.rotadamemoria.data.model.UserResult
import titoaesj.br.com.rotadamemoria.data.sharedpreference.SessionHandler
import java.io.IOException
import java.net.HttpURLConnection

/**
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 04/08/2018.
 */
class AuthInterceptor(private val context: Context) : Interceptor {

    companion object {
        val TAG: String = AuthInterceptor::class.java.simpleName
    }


    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        var newRequest = request
        val response: Response

        /**
         * Se o token for registrado cria um novo Request
         */
        if (!SessionHandler.getToken(context).isNullOrEmpty()) {
            newRequest = request.newBuilder()
                    .addHeader("Authorization", "Bearer ${SessionHandler.getToken(context)}")
                    .build()
        }

        response = chain.proceed(newRequest)

        /**
         * Verifica se o token não é mais válido
         * se não for chama o método para renova-lo
         */
        if (response.code() == HttpURLConnection.HTTP_UNAUTHORIZED) {
            Timber.tag(TAG).d("chamou o refresh")
            this.refreshOAuth()
        }

        return response
    }

    /**
     * Método para renovação do token
     */
    @Synchronized
    private fun refreshOAuth() {

        val client = OkHttpClient()

        val requestBody = MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("token", SessionHandler.getToken(context)!!)
                .build()

        val request = Request.Builder()
                .url("http://rota-memoria.thitans.com/api/refresh")
                .post(requestBody)
                .build()

        var response: Response? = null
        try {
            response = client.newCall(request).execute()
            if (response!!.code() != HttpURLConnection.HTTP_UNAUTHORIZED) {
                val accessToken = Gson().fromJson(response.body()!!.string(), TokenResult::class.java)
                SessionHandler.removeToken(context)
                SessionHandler.setToken(context, accessToken.token!!)
                Timber.tag(TAG).e( "token refreshado: " + response.body())
            } else {
                Timber.tag(TAG).e( "refreshOAuth: Erro no refresh " + response.body()!!.string())
            }
        } catch (e: IOException) {
            Timber.tag(TAG).e("refreshOAuth: Erro no refresh " + e.message)
        }

    }
}

