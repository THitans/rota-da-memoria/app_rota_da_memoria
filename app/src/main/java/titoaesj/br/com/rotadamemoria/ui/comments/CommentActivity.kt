package titoaesj.br.com.rotadamemoria.ui.comments

import android.content.Context
import android.graphics.Bitmap
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.InputType
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.ProgressBar
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import com.squareup.picasso.Picasso
import com.squareup.picasso.Transformation
import kotlinx.android.synthetic.main.activity_comment.*
import kotlinx.android.synthetic.main.activity_sub_topic.*
import kotlinx.android.synthetic.main.base_layout_appbarlayout.*
import kotlinx.android.synthetic.main.base_layout_appbarlayout.view.*
import timber.log.Timber
import titoaesj.br.com.rotadamemoria.R
import titoaesj.br.com.rotadamemoria.data.sharedpreference.SessionHandler
import titoaesj.br.com.rotadamemoria.helper.startActivityNoHistory
import titoaesj.br.com.rotadamemoria.ui.auth.SigninActivity

class CommentActivity : AppCompatActivity(), CommentView {

    companion object {
        val TAG: String = CommentActivity::class.java.simpleName
    }

    private val kodein by lazy { LazyKodein(appKodein) }
    private val mPresenter: CommentPresenter by kodein.with(this).instance()

    private var rvAdapterComment : RVAdapterComment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comment)

        setSupportActionBar(activity_comment_appbarlayout.toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        activity_comment_appbarlayout.toolbar.setNavigationIcon(R.drawable.ic_close)


        val fileId = intent.getIntExtra(getString(R.string.TAG_BUNDLE_FILE_ID), 0)
        val filePath = intent.getStringExtra(getString(R.string.TAG_BUNDLE_FILE_PATH))


        Picasso.with(this)
                .load("http://rota-memoria.thitans.com/storage/${filePath}")
                .fit()
                .centerCrop()
                .into(activity_comment_cover)


        activity_comment_recyclerview.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        rvAdapterComment = RVAdapterComment()
        CommentListField().adapter = rvAdapterComment

        mPresenter.getUserInSQLite(fileId)

        activity_comment_fab.setOnClickListener {
            val builder: AlertDialog.Builder = AlertDialog.Builder(this)
            builder.setTitle("Comentário")
            val input = EditText(this)
            input.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_MULTI_LINE
            builder.setView(input)


            builder.setPositiveButton("Enviar") { p0, _ ->
                Timber.tag(TAG).d("Comentário ${input.text.toString()}")
                Timber.tag(TAG).d("fileId ${fileId.toString()}")

                if (fileId != 0) {
                    mPresenter.storageCommentAction(input.text.toString(), fileId)
                }

            }


            builder.setNegativeButton("Cancelar") { p0, _ ->
                p0.cancel()
            }

            builder.show();
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = MenuInflater(this)
        inflater.inflate(R.menu.logout_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            R.id.menu_logout -> {
                SessionHandler.removeToken(this)
                SessionHandler.removeUserID(this)
                mPresenter.removeUserInSQLite()
                startActivityNoHistory<SigninActivity>()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun UsernameField(): AppCompatTextView = base_layout_appbarlayout_username
    override fun CommentListField(): RecyclerView = activity_comment_recyclerview
    override fun Context(): Context = this
    override fun loadingField(): ProgressBar = activity_comment_loading_progressbar
    override fun CommentAdapter(): RVAdapterComment? = rvAdapterComment
}
