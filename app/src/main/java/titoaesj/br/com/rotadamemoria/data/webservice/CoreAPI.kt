package titoaesj.br.com.rotadamemoria.data.webservice

import com.google.gson.JsonObject
import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.http.*
import titoaesj.br.com.rotadamemoria.data.model.*

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 02/08/2018.
 */
interface CoreAPI {

    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("./topics")
    fun fetchTopics(): Observable<List<Topic>>


    @Headers("Content-Type: application/json", "Accept: application/json")
    @GET("./comment/list/{fileId}")
    fun fetchComments(@Path("fileId") fileId: Int): Observable<CommentResult>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("./comment/store")
    fun storageComment(@Body data: Comment): Observable<Result>


    @Headers("Accept: application/json")
    @Multipart
    @POST("./upload-image")
    fun upload(@Part file : MultipartBody.Part,
               @Part("sub_topic_id") subTopicId: Int): Observable<JsonObject>


    @Headers("Accept: application/json")
    @GET("./list-images/{subTopicId}")
    fun galery(@Path("subTopicId") subTopicId: Int): Observable<Galery>


}