package titoaesj.br.com.rotadamemoria.ui.auth

import android.content.Context
import android.support.design.widget.TextInputLayout

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 21/07/2018.
 */
interface SigninView {
    fun Context(): Context
    fun EmailFiled(): TextInputLayout
    fun PasswordField(): TextInputLayout
}