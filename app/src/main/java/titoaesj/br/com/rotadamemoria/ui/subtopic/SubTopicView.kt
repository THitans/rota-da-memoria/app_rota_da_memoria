package titoaesj.br.com.rotadamemoria.ui.subtopic

import android.content.Context
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 03/08/2018.
 */
interface SubTopicView {
    fun Context(): Context
    fun SubTopicListField(): RecyclerView
    fun UsernameField(): AppCompatTextView
    fun SubTopicAdapter(): RVAdapterSubTopic?
}