package titoaesj.br.com.rotadamemoria.ui.comments

import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.TextStyle
import titoaesj.br.com.rotadamemoria.R
import titoaesj.br.com.rotadamemoria.data.model.Comment
import java.util.*


/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 31/08/2018.
 */
class RVAdapterComment : RecyclerView.Adapter<RVAdapterComment.ViewHolder>() {


    private val itensOfList: MutableList<Comment> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RVAdapterComment.ViewHolder {
        val v = LayoutInflater
                .from(parent.context)
                .inflate(R.layout.recyclerview_comment_item, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: RVAdapterComment.ViewHolder, position: Int) {
        holder.bindItems(itensOfList[position])
    }

    override fun getItemCount(): Int {
        return itensOfList.size
    }

    fun clearAdapter() {
        itensOfList.clear()
        notifyDataSetChanged()
    }

    fun addItemAdapter(item: Comment) {
        itensOfList.add(item)
        notifyItemChanged(itensOfList.size - 1)
    }

    fun setAdapter(itens: List<Comment>) {
        itensOfList.clear()
        itensOfList.addAll(itens)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(item: Comment) {

            val text  = itemView.findViewById<AppCompatTextView>(R.id.recyclerview_topic_item_title)
            val author  = itemView.findViewById<AppCompatTextView>(R.id.recyclerview_comment_item_author)

            text.text = item.text
            val BRAZIL = Locale("pt", "BR")
            val date = if(item.updateddAt.isNullOrEmpty()) item.createddAt else item.updateddAt
            val formatter : DateTimeFormatter  = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
            val dateFinal = LocalDateTime.parse(date,formatter)

            author.text = "${item.user?.name} " +
                    "em ${dateFinal.dayOfMonth} " +
                    "de ${dateFinal.month.getDisplayName(TextStyle.FULL, BRAZIL)} " +
                    "de ${dateFinal.year}"


        }
    }
}
