package titoaesj.br.com.rotadamemoria.ui.galery

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.ProgressBar
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import com.tbruyelle.rxpermissions2.RxPermissions
import kotlinx.android.synthetic.main.activity_galery.*
import kotlinx.android.synthetic.main.base_layout_appbarlayout.*
import kotlinx.android.synthetic.main.base_layout_appbarlayout.view.*
import timber.log.Timber
import titoaesj.br.com.rotadamemoria.R
import titoaesj.br.com.rotadamemoria.data.sharedpreference.SessionHandler
import titoaesj.br.com.rotadamemoria.helper.*
import titoaesj.br.com.rotadamemoria.ui.auth.SigninActivity
import titoaesj.br.com.rotadamemoria.ui.comments.CommentActivity
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class GaleryActivity : AppCompatActivity(), GaleryView {

    companion object {
        val TAG: String = GaleryActivity::class.java.simpleName
    }

    private val REQUEST_IMAGE_CAPTURE = 12
    private val REQUEST_IMAGE_MEDIA = 1
    private val JPEG_FILE_PREFIX = "IMG_"
    private val JPEG_FILE_SUFFIX = ".jpg"
    private var mAlbumStorageDirFactory: AlbumStorageDirFactory? = null
    private var mCurrentPhotoUri: Uri? = null
    private var mCurrentPhotoPath: String? = null
    private var mAbsolutePath: RealPathUtil? = null
    private lateinit var mAdapter: RVAdapterGalery

    private val kodein by lazy { LazyKodein(appKodein) }
    private val mPresenter: GaleryPresenter by kodein.with(this).instance()

    private var subtopicId: Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_galery)

        val titleGalery = intent.getStringExtra(resources.getString(R.string.TAG_BUNDLE_GALERY_TITLE))
                ?: ""

        subtopicId = intent.getIntExtra(resources.getString(R.string.TAG_BUNDLE_TOPIC_ID), 0)


        mPresenter.getUserInSQLite()

        activity_galery_appbarlayout.toolbar.title = titleGalery
        setSupportActionBar(activity_galery_appbarlayout.toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        activity_galery_appbarlayout.toolbar.setNavigationIcon(R.drawable.ic_close)
        activity_galery_appbarlayout.toolbar.navigationIcon?.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP)


        // Define a layout for RecyclerView
        val mLayoutManager = GridLayoutManager(this, 2)
        activity_galery_recyclerview.layoutManager = mLayoutManager

        val itemDecoration = GridInsetDecoration(this)
        activity_galery_recyclerview.addItemDecoration(itemDecoration)

        // Initialize a new instance of RecyclerView Adapter instance
        mAdapter = RVAdapterGalery()


        // Set the adapter for RecyclerView
        activity_galery_recyclerview.adapter = mAdapter


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            mAlbumStorageDirFactory = FroyoAlbumDirFactory()
        } else {
            mAlbumStorageDirFactory = BaseAlbumDirFactory()
        }

        menu_item_camera.setOnClickListener {


            RxPermissions(this)
                    .request(
                            Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                    .subscribe {
                        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        if (takePictureIntent.resolveActivity(packageManager) != null) {
                            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
                        }
                    }


        }

        menu_item_galery.setOnClickListener {

            RxPermissions(this)
                    .request(
                            Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                    .subscribe {
                        val intent = Intent()
                        intent.type = "image/*"
                        intent.action = Intent.ACTION_GET_CONTENT
                        startActivityForResult(intent, REQUEST_IMAGE_MEDIA)
                        menu.close(true)
                    }
        }
    }


    override fun onStart() {
        super.onStart()
        mPresenter.loadGalery(subtopicId)
    }

    fun launchToCommentActivity(item: titoaesj.br.com.rotadamemoria.data.model.File) {
        val bundle = Bundle()
        bundle.putInt(getString(R.string.TAG_BUNDLE_FILE_ID), item.id)
        bundle.putString(getString(R.string.TAG_BUNDLE_FILE_PATH), item.pathFile)

        startActivity<CommentActivity>(bundle)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        /**
         * Inicia uma instância do RealPath
         */
        mAbsolutePath = RealPathUtil

        /**
         * Busca imagem salva no aparelho.
         */
        if (requestCode == REQUEST_IMAGE_MEDIA) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    if (!RealPathUtil.isGoogleDrivePhotosUri(data.data)) {

                        val realPath = mAbsolutePath!!.getRealPath(this, data.data)

                        if (realPath != "mediakey") {

                            val uriFromPath = Uri.fromFile(File(realPath))


                            val mBitmap: Bitmap? = loadBitmapFromUri(uriFromPath)
                            mBitmap.let { mPresenter.upload(mBitmap!!, 1) }

                            mPresenter.upload(mBitmap!!, subtopicId)
//                            mAdapter.addItemAdapter(mBitmap!!)


                        } else {
                            val mBitmap = loadBitmapFromUri(data.data)
                            if (mBitmap != null) {
//                                mAdapter.addItemAdapter(mBitmap)
                                mPresenter.upload(mBitmap, subtopicId)
                            }
                        }
                    } else {
                        val mBitmap = loadBitmapFromUri(data.data)
                        if (mBitmap != null) {
                            mPresenter.upload(mBitmap, subtopicId)
//                            mAdapter.addItemAdapter(mBitmap)
                        }
                    }
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Timber.tag(TAG).d("onActivityResult: canceled")
            }
        }


        /**
         * Chama o aplicativo de camera do aparelho e salva a imagem no SDCard.
         */
        if (requestCode == REQUEST_IMAGE_CAPTURE
                && resultCode == RESULT_OK) {


            val extras: Bundle = data!!.extras
            val imageBitmap: Bitmap = extras.get("data") as Bitmap

//            mAdapter.addItemAdapter(imageBitmap)

            mPresenter.upload(imageBitmap, subtopicId)


        }
    }

    @Throws(IOException::class)
    private fun setUpPhotoFile(): File {
        return createImageFile()
    }

    /**
     * Create a Media for saving an image
     */
    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
        val imageFileName = JPEG_FILE_PREFIX + timeStamp + "_"
        val albumF = getAlbumDir()
        return File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF)
    }

    private fun getAlbumDir(): File? {
        var storageDir: File? = null

        if (Environment.MEDIA_MOUNTED == Environment.getExternalStorageState()) {
            storageDir = mAlbumStorageDirFactory!!.getAlbumStorageDir(getAlbumName())

            if (!storageDir.mkdirs()) {
                if (!storageDir.exists()) {
                    Log.d("CameraSample", "failed to create directory")
                    return null
                }
            }

        } else {
            Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.")
        }
        return storageDir
    }

    private fun getAlbumName(): String {
        return getString(R.string.album_name)
    }

    fun loadBitmapFromUri(uri: Uri?): Bitmap? {
        try {
            val input = contentResolver.openInputStream(uri)
            return BitmapFactory.decodeStream(input)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }

        return null
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater : MenuInflater = MenuInflater(this)
        inflater.inflate(R.menu.logout_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            R.id.menu_logout -> {
                SessionHandler.removeToken(this)
                SessionHandler.removeUserID(this)
                mPresenter.removeUserInSQLite()
                startActivityNoHistory<SigninActivity>()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun Context(): Context {
        return this
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        mPresenter.cancelRequest()
    }

    override fun Adapter(): RVAdapterGalery {
        return mAdapter
    }

    override fun SubTopicId(): Int {
        return subtopicId
    }

    override fun loadingField(): ProgressBar {
        return activity_galery_loading_progressbar
    }

    override fun UsernameField(): AppCompatTextView {
        return base_layout_appbarlayout_username
    }
}
