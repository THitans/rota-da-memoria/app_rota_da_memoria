package titoaesj.br.com.rotadamemoria.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 21/07/2018.
 */
data class SubTopic(

        @SerializedName("id")
        var id: Int = 0,

        @SerializedName("topic_id")
        var topicId: Int = 0,

        @SerializedName("text")
        var title: String = "",

        @SerializedName("is_published")
        var isPublished: Boolean = true,

        @SerializedName("created_at")
        var createdAt: String = "",

        @SerializedName("updated_at")
        var updatedAt: String = "",

        @SerializedName("deleted_at")
        var deletedAt: String = ""

) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readString(),
            parcel.readString(),
            (parcel.readString() != "").toString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeInt(topicId)
        parcel.writeString(title)
        parcel.writeByte(if (isPublished) 1 else 0)
        parcel.writeString(createdAt)
        parcel.writeString(updatedAt)
        parcel.writeString(deletedAt)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SubTopic> {
        override fun createFromParcel(parcel: Parcel): SubTopic {
            return SubTopic(parcel)
        }

        override fun newArray(size: Int): Array<SubTopic?> {
            return arrayOfNulls(size)
        }
    }


}