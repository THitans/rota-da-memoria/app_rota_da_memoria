package titoaesj.br.com.rotadamemoria.data.webservice

import io.reactivex.Observable
import retrofit2.http.*
import titoaesj.br.com.rotadamemoria.data.model.User
import titoaesj.br.com.rotadamemoria.data.model.UserResult

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 21/07/2018.
 */
interface AuthAPI {

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("./login")
    fun login(@Body data: User): Observable<UserResult>

    @Headers("Content-Type: application/json", "Accept: application/json")
    @POST("./refresh")
    fun refresh(@Part token: String): Observable<UserResult>

}