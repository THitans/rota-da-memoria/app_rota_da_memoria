package titoaesj.br.com.rotadamemoria.ui.auth

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import titoaesj.br.com.rotadamemoria.data.repository.AuthRepository
import titoaesj.br.com.rotadamemoria.data.repository.UserRepository

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 21/07/2018.
 */
class SigninViewModelFactory(private val authRepository: AuthRepository,
                             private val userRepository: UserRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SigninViewModel::class.java)) {
            return SigninViewModel(authRepository, userRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}