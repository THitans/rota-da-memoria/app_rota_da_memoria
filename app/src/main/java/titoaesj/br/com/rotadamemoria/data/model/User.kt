package titoaesj.br.com.rotadamemoria.data.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 21/07/2018.
 */
@Entity(
        tableName = "user"
)
class User(

        @PrimaryKey(autoGenerate = true)
        var uid: Long = 0L,


        @SerializedName("id")
        @ColumnInfo(name = "id_user")
        var id: Int? = 0,


        @SerializedName("name")
        @ColumnInfo(name = "name")
        var name: String? = "",


        @SerializedName("email")
        @ColumnInfo(name = "email")
        var email: String = "",


        @SerializedName("password")
        @ColumnInfo(name = "password")
        var password: String? = ""

) {
        override fun toString(): String {
                return "\tUser( \nId: $id \nNome: $name \nEmail: $email \nSenha: $password \n\n"
        }
}