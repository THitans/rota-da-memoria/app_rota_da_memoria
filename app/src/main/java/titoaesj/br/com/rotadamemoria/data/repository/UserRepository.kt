package titoaesj.br.com.rotadamemoria.data.repository

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import titoaesj.br.com.rotadamemoria.data.model.User
import titoaesj.br.com.rotadamemoria.data.sqlite.dao.UserDAO

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 31/08/2018.
 */
class UserRepository(private val userDAO: UserDAO) {


    fun storageUserInSQLite(user: User): Observable<Unit> =
            Observable.fromCallable { userDAO.insert(user) }
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())


    fun removeAllUserInSQLite(): Observable<Unit> =
            Observable.fromCallable { userDAO.deleteAll() }
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())


    fun getUserInSQLite(): Observable<User> =
            Observable.fromCallable { userDAO.user() }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
}