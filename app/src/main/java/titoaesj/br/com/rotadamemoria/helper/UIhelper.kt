package titoaesj.br.com.rotadamemoria.helper

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.TextView
import titoaesj.br.com.rotadamemoria.R

/**
 *
 * Project URPay
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 20/07/2018.
 */


inline fun CoordinatorLayout.showSnackMsg(message: String, length: Int = Snackbar.LENGTH_LONG) {
    val snackbar = Snackbar.make(this,
            message,
            length)

//    val sbView = snackbar.view
//    val sbText = sbView.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView

//    sbView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorSnackbarBg))
//    sbText.setTextColor(ContextCompat.getColor(context, R.color.colorSnackbarText))

    snackbar.show()
}

/**
 * Monta o Snackbar de network check
 */
inline fun View.snackNetworkCheck(coordinatorLayout: CoordinatorLayout): Snackbar {

    val snackbar = Snackbar.make(coordinatorLayout,
            context.getString(R.string.snackbar_without_cloud_off),
            Snackbar.LENGTH_INDEFINITE)

    val sbView = snackbar.view
    val sbText = sbView.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView

    sbView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorSnackbarBg))
    sbText.setTextColor(ContextCompat.getColor(context, R.color.colorSnackbarText))

    return snackbar
}

inline fun <reified T : Activity> Activity.startActivity(extra: Bundle? = null) {

    val it =  Intent(this, T::class.java)
    if(extra != null) { it.putExtras(extra) }
    startActivity(it
            .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
}
inline fun <reified T : Activity> Activity.startActivityNoHistory(extra : Bundle? = null) {

    val it =  Intent(this, T::class.java)
    if(extra != null) { it.putExtras(extra) }
    startActivity(it
            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            .addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY))
    finish()
}