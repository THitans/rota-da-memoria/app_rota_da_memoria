package titoaesj.br.com.rotadamemoria.ui.topic

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import titoaesj.br.com.rotadamemoria.R
import titoaesj.br.com.rotadamemoria.data.model.Topic
import titoaesj.br.com.rotadamemoria.ui.subtopic.SubTopicActivity

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 21/07/2018.
 */
class RVAdapterTopic : RecyclerView.Adapter<RVAdapterTopic.ViewHolder>() {

    companion object {
        val TAG: String = RVAdapterTopic::class.java.simpleName
    }

    private val itensOfList: MutableList<Topic> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RVAdapterTopic.ViewHolder {
        val v = LayoutInflater
                .from(parent.context)
                .inflate(R.layout.recyclerview_topic_item, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: RVAdapterTopic.ViewHolder, position: Int) {
        holder.bindItems(itensOfList[position])
    }

    override fun getItemCount(): Int {
        return itensOfList.size
    }

    fun clearAdapter() {
        itensOfList.clear()
        notifyDataSetChanged()
    }

    fun addItemAdapter(item: Topic) {
        itensOfList.add(item)
        notifyItemChanged(itensOfList.size - 1)
    }

    fun setAdapter(itens: List<Topic>) {
        itensOfList.clear()
        itensOfList.addAll(itens)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(topic: Topic) {

            val button = itemView.findViewById<AppCompatTextView>(R.id.recyclerview_topic_item_title)
            val cardView = itemView.findViewById<CardView>(R.id.recyclerview_topic_item_cardview)
            button.text = topic.title

            cardView.setOnClickListener {

                val extra = Bundle()
                extra.putParcelable(itemView.context.getString(R.string.TAG_BUNDLE_TOPIC), topic)

                val intent = Intent(itemView.context, SubTopicActivity::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .putExtras(extra)

                itemView.context.startActivity(intent)
            }
        }
    }
}
