package titoaesj.br.com.rotadamemoria.data.webservice

import android.content.Context
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 21/07/2018.
 */

object WebServiceFactory {

    private const val DEFAULT_TIMEOUT_SECONDS = 15L
    private const val BASE_URL = "http://rota-memoria.thitans.com/api/"

    fun build(apiURL: String = BASE_URL,
              debuggable: Boolean = false,
              context: Context): Retrofit {

        val logger = createLogger(debuggable)
        val httpClient = createHttpClient(logger = logger, context = context)
        val converter = GsonConverterFactory.create()
        val rxAdapter = RxJava2CallAdapterFactory.create()

        return Retrofit.Builder()
                .baseUrl(apiURL)
                .client(httpClient)
                .addConverterFactory(converter)
                .addCallAdapterFactory(rxAdapter)
                .build()
    }

    private fun createLogger(debuggable: Boolean): Interceptor {

        val loggingLevel = if (debuggable)
            HttpLoggingInterceptor.Level.BODY
        else
            HttpLoggingInterceptor.Level.NONE

        return HttpLoggingInterceptor().apply { level = loggingLevel }
    }

    private fun createHttpClient(logger: Interceptor, context: Context): OkHttpClient {

        return OkHttpClient.Builder()
                .addInterceptor(AuthInterceptor(context))
                .addInterceptor(logger)
                .connectTimeout(DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .readTimeout(DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .build()
    }

}