package titoaesj.br.com.rotadamemoria.ui.topic

import android.support.v7.app.AlertDialog
import android.text.TextUtils
import android.view.View
import com.google.gson.GsonBuilder
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import retrofit2.HttpException
import timber.log.Timber
import titoaesj.br.com.rotadamemoria.data.model.UserResult
import titoaesj.br.com.rotadamemoria.data.sharedpreference.SessionHandler
import titoaesj.br.com.rotadamemoria.helper.startActivityNoHistory
import titoaesj.br.com.rotadamemoria.ui.auth.SigninActivity
import titoaesj.br.com.rotadamemoria.ui.auth.SigninPresenter

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 03/08/2018.
 */
class TopicPresenter(view: Any, model: TopicViewModel) {

    companion object {
        val TAG: String = SigninPresenter::class.java.simpleName
    }

    private val mView: TopicView = view as TopicView
    private val mViewModel: TopicViewModel = model

    private val compositeDisposable = CompositeDisposable()
    private var disposable: Disposable? = null

    fun cancelRequest() {
        compositeDisposable.clear()
    }

    fun getTopicsAction() {

        Timber.tag(TAG).d("getTopicsAction")
        startLoading()

        disposable = mViewModel.fetchTopicsAction()
                .subscribe(
                        { payload ->

                            Timber.tag(TAG).d("payload title ${payload}")
                            mView.TopicAdapter()?.setAdapter(payload)

                            stopLoading()
                        },
                        { error ->

                            try {
                                val mapError = error as HttpException

                                if (mapError.code() == 401) {
                                    SessionHandler.removeToken(mView.Context())
                                    SessionHandler.removeUserID(mView.Context())
                                    removeUserInSQLite()
                                    (mView.Context() as TopicActivity).startActivityNoHistory<SigninActivity>()
                                } else {

                                    val errorBody = mapError.response().errorBody()?.string()

                                    val gson = GsonBuilder().setPrettyPrinting().create()
                                    val userResult = gson.fromJson<UserResult>(errorBody, UserResult::class.java)

                                    if (!TextUtils.equals(userResult.message, "Token Expirado! Token has expired")) {
                                        val simpleAlert = AlertDialog.Builder(mView.Context()).create()
                                        simpleAlert.setTitle("Ops, Ocorreu um error")
                                        simpleAlert.setMessage(userResult.message)
                                        simpleAlert.setButton(AlertDialog.BUTTON_POSITIVE, "OK") { _, i ->
                                        }
                                        simpleAlert.show()

                                    }
                                }
                            } catch (e: Exception) {
                                Timber.tag(TAG).e("getTopicsAction ${e.message}")
                            }

                            stopLoading()
                        }
                )

        if (disposable != null) {
            compositeDisposable.add(disposable!!)
        }
    }

    private fun startLoading() {
        mView.loadingField().visibility = View.VISIBLE
    }

    private fun stopLoading() {
        mView.loadingField().visibility = View.GONE
    }

    fun removeUserInSQLite() {
        mViewModel.removeUserInSQLite()
    }

    fun getUserInSQLite() {
        disposable = mViewModel.getUserInSQLite()
                .subscribe {
                    Timber.tag(TAG).d("getUserInSQLite ${it.toString()}")
                    mView.UsernameField().text = it.name
                }

        if (disposable != null) {
            compositeDisposable.add(disposable!!)
        }
    }


}

