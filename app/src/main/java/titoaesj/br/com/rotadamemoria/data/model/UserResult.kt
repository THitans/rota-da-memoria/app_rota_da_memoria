package titoaesj.br.com.rotadamemoria.data.model

import com.google.gson.annotations.SerializedName

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 21/07/2018.
 */
class UserResult(

        @SerializedName("token")
        var token: String? = "",

        @SerializedName("user")
        var user: User?,

        @SerializedName("message")
        var message: String = ""

)