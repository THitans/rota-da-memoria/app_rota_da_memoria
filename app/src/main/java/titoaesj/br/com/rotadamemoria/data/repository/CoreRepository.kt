package titoaesj.br.com.rotadamemoria.data.repository

import com.google.gson.JsonObject
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody
import titoaesj.br.com.rotadamemoria.data.model.*
import titoaesj.br.com.rotadamemoria.data.webservice.CoreAPI

/**
 *
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 02/08/2018.
 */
class CoreRepository(
        val coreAPI: CoreAPI) {

    /**
     * Realiza o login do usuário
     * utilizando Uusuário e Senha
     */
    fun fetchTopics(): Observable<List<Topic>> =
            coreAPI.fetchTopics()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .retry()


    fun storageCommentAction(data: Comment): Observable<Result> = coreAPI.storageComment(data)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())


    fun fetchComments(fileId : Int): Observable<CommentResult> =
            coreAPI.fetchComments(fileId)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .retry()


    fun upload(file: MultipartBody.Part, subTopicId: Int): Observable<JsonObject> = coreAPI.upload(file, subTopicId)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())


    fun galery(subTopicId: Int): Observable<Galery> = coreAPI.galery(subTopicId)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .retry()

}