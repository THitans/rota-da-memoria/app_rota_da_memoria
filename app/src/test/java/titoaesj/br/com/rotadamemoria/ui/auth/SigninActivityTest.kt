package titoaesj.br.com.rotadamemoria.ui.auth

import android.support.v7.widget.AppCompatButton
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import titoaesj.br.com.rotadamemoria.R


/**
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 26/07/2018.
 */
@RunWith(RobolectricTestRunner::class)
class SigninActivityTest {

    @Before
    fun setUp() {
    }

    @Test
    fun clickingLogin_shouldStartLoginActivity() {
        val activity = Robolectric.setupActivity(SigninActivity::class.java)
        activity.findViewById<AppCompatButton>(R.id.activity_signin_appcompatbutton_signin).performClick()
    }
}