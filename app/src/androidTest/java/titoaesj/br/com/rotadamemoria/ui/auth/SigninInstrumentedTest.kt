package titoaesj.br.com.rotadamemoria.ui.auth

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withText
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import titoaesj.br.com.rotadamemoria.R

/**
 * Project RotadaMemoria
 * Desenvolvido por Tito Albino Evangelista da Silva Junior em 25/07/2018.
 */
@RunWith(AndroidJUnit4::class)
class SigninInstrumentedTest {


    @get:Rule
    var mActivity = ActivityTestRule(SigninActivity::class.java)


    @Test
    fun whenEmailIsEmptyTest() {

        onView(ViewMatchers.withId(R.id.activity_signin_textinputedittext_email))
                .perform(ViewActions
                        .typeText(""), ViewActions.closeSoftKeyboard())

        onView(ViewMatchers.withId(R.id.activity_signin_textinputedittext_password))
                .perform(ViewActions
                        .typeText("123456789"), ViewActions.closeSoftKeyboard())

        onView(ViewMatchers.withId(R.id.activity_signin_appcompatbutton_signin)).perform(ViewActions.click())

        onView(withText(R.string.empty_message_default)).check(matches(isDisplayed()))

    }

    @Test
    fun whenPasswordIsEmptyTest() {
        onView(ViewMatchers.withId(R.id.activity_signin_textinputedittext_email))
                .perform(ViewActions
                        .typeText("tito"), ViewActions.closeSoftKeyboard())

        onView(ViewMatchers.withId(R.id.activity_signin_textinputedittext_password))
                .perform(ViewActions
                        .typeText(""), ViewActions.closeSoftKeyboard())

        onView(ViewMatchers.withId(R.id.activity_signin_appcompatbutton_signin)).perform(ViewActions.click())

        onView(withText(R.string.empty_message_default)).check(matches(isDisplayed()))
    }

}
